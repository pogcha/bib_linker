# file name without extension
fileName = "myBib"
fileName = input('Enter file name of .bib file (with out extension): ')
if fileName[-4:] == '.bib':
    raise Exception('Give filename with out extension')
    
# choose if link is inserted in year or title
# year is maybe the better choice. Latex has problems in line breaking long
# links. It seems to work... but once a link is line broken the hyperlink is 
# lost
insertIn = input('Enter \'year\' or \'title\' to insert link on year or title (Enter for \'year\'):')
if len(insertIn) == 0:
    insertIn = 'year'
#insertIn = 'year'
#insertIn = 'title'

# check if 'insertIn' is chosen correctly
if insertIn != 'year' and insertIn != 'title':
    raise Exception('Give \'year\' or \'title\'')

print('Adding doi/url (in that order) to \''+insertIn+'\' in file '+fileName+'.bib')

def extract(string):
    # extract string between first appearance of {
    # and last appearance of }
    firstPos = string.find('{')
    lastPos = string[-1::-1].find('}')
    ex = string[firstPos+1:len(string)-lastPos-1]
    return ex
    
def href(title,doi,isDOI):
    # merge title and url(doi) to form a href command
    if isDOI:
        urlAdress = '\href{http://dx.doi.org/'+doi+'}{'+title+'}'
    else:
        urlAdress = '\href{'+doi+'}{'+title+'}'
    return urlAdress
    
def makeEntry(entryName,entry):
    # make a bibtex entry
    return('    '+entryName+' = {'+entry+'},\n')
 
# open original bib file
print('reading original .bib ...')
f = open(fileName+'.bib','r')
lines = f.readlines()
f.close()

print('processing original .bib ...')
# loop over all lines and get start line of each entry
itemStart = []
for iLine in range(len(lines)):
    if lines[iLine][0] == '@':
        itemStart.append(iLine)
        
# make list with start line and end line of each item
iterLines = []
for ii in range(len(itemStart)-1):
    iterLines.append([itemStart[ii],itemStart[ii+1]])
iterLines.append([itemStart[-1],len(lines)])

# allocate some lists in which we store the relevant entries and
# their line numbers
titles = []
years = []
dois = []
urls = []
titlesLines = []
yearsLines = []

# loop over each item and get the relevant entries and line numbers
# if no item is found, we add a None to the list
# in that way we can be sure, that the lists have the same amount of entries
# as the number of items

for iItem in range(len(itemStart)):
    go = True
    tit = False
    doi = False
    url = False
    year = False
    for ii in range(iterLines[iItem][0],iterLines[iItem][1]):
        splitter = lines[ii].split()
        if len(splitter) > 0:
            if splitter[0] == 'title':
                titles.append(extract(lines[ii]))
                titlesLines.append(ii)
                tit = True
            if splitter[0] == 'doi':
                dois.append(extract(lines[ii]))
                doi = True
            if splitter[0] == 'url':
                urls.append(extract(lines[ii]))
                url = True
            if splitter[0] == 'year':
                years.append(extract(lines[ii]))
                yearsLines.append(ii)
                year = True
    
    # add dummy entries
    if tit == False:
        titles.append(None)
        titlesLines.append(None)
    if doi == False:
        dois.append(None)
    if url == False:
        urls.append(None)
    if year == False:
        years.append(None)
        yearsLines.append(None)

# write out new .bib
print('writing original .bib ...')
f = open(fileName+'DOI.bib','w')

if insertIn == 'title':
    insertThis = titles
    insertLine = titlesLines
else:
    insertThis = years
    insertLine = yearsLines
    
for iItem in range(len(itemStart)):
    for ii in range(iterLines[iItem][0],iterLines[iItem][1]):
        # replace entry with entry+DOI-link if title and doi are available
        if dois[iItem] != None and insertThis[iItem] != None:
            if ii == insertLine[iItem]:
                f.write(makeEntry(insertIn,href(insertThis[iItem],dois[iItem],True)))
            else:
                f.write(lines[ii])
        # replace entry with entry+URL-link if title and url are available
        elif urls[iItem] != None and insertThis[iItem] != None:
            if ii == insertLine[iItem]:
                f.write(makeEntry(insertIn,href(insertThis[iItem],urls[iItem],False),))
            else:
                f.write(lines[ii])
        # Do nothing if no url and no DOI are available
        else:
            f.write(lines[ii])
f.close()
print('finished')


